#include <stdio.h>

struct fraction{
    int num;
    int den;
};

struct fraction scan(){
    struct fraction a;
    printf("Enter values of numerator and denominator: ");
    scanf("%d %d", &a.num, &a.den);
    return a;
}

struct fraction compute_one(struct fraction f1, struct fraction f2){
    struct fraction res;
    res.den = f1.den * f2.den;
    res.num = (f1.num * f2.den) + (f2.num * f1.den);
    return res;
}

int gcd(int num, int den){
    int lcm;
    for (int i=1; i<=num && i<=den; i++){
        if (num%i==0 && den%i==0){
            lcm=i;
        }
    }
    return lcm;
}

struct fraction reduce_fraction(struct fraction f, int lcm){
    f.num /= lcm;
    f.den /= lcm;
    return f;
}

struct fraction total_sum(struct fraction arr[], int size){
    struct fraction compute_n;
    compute_n.num = 0;
    compute_n.den = 1;

    for (int i = 0; i < size; i++)
        compute_n = compute_one(compute_n, arr[i]);

    int lcm = gcd(compute_n.num, compute_n.den);
    compute_n = reduce_fraction(compute_n, lcm);

    return compute_n;
}

int main(){
    int n;
    printf("Enter the number of fractions: ");
    scanf("%d", &n);
    struct fraction frac[n];
    for (int i = 0; i< n; i++){
        frac[i] = scan();
    }
    struct fraction result;
    result = total_sum(frac, n);
    printf("The reduced fraction is: %d/%d", result.num, result.den);
    return 0;
}