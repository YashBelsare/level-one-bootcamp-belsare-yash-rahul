//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
float scan(float x);
float compute(float x, float y, float z);
float print(float x);
int main(){
    float h,d,b, h_scan, d_scan, b_scan, volume;
    h = scan(h_scan);
    d = scan(d_scan);
    b = scan(b_scan);
    volume = compute(h, d, b);
    print(volume);
    return 0;
}
float scan(float x){
    scanf("%f", &x);
    return x;
}
float compute(float x, float y, float z){
    float vol = (1.0/3.0)*(((x*y) + y) / z);
    return vol;
}
float print(float x){
    printf("Volume of Tromboloid: %.2f", x);
}