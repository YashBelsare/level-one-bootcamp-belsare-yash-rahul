//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include <math.h>
float scan(float x);
float compute(float x1, float x2, float y1, float y2);
float print(float x, float, float, float, float);
int main(){
    float x1, x1_scan, x2, x2_scan, y1, y1_scan, y2, y2_scan, dist;
    printf("Enter co-ordinates of 1st point (x,y):\n");
    x1 = scan(x1_scan);
    y1 = scan(y1_scan);
    printf("Enter co-ordinates of 2nd point (x,y):\n");
    x2 = scan(x2_scan);
    y2 = scan(y2_scan);
    dist = compute(x1, x2, y1, y2);
    print(dist, x1, y1, x2, y2);
    return 0;
}
float scan(float x){
    scanf("%f", &x);
    return x;
}
float compute(float x1, float x2, float y1, float y2){
    float dist = sqrt(((y2 - y1)*(y2-y1)) + ((x2 - x1)*(x2-x1)));
    return dist;
}
float print(float x, float x1, float y1, float x2, float y2){
    printf("Distance between point (%.2f, %.2f) and (%.2f, %.2f) is %.2f", x1, y1, x2, y2, x);
}