#include <stdio.h>

struct fraction{
    int num, den;
};

struct fraction scan (struct fraction a){
    printf("Enter value of numerator and denominator: ");
    scanf("%d %d", &a.num, &a.den);
return a;
}

struct fraction compute(struct fraction a, struct fraction b){
    struct fraction res;
    res.num = (a.num*b.den)+(a.den*b.num);
    res.den = a.den*b.den;
    return res;
}

int gcd (int num, int den){
    int i,gcd;
    for(i=0; i <= num && i <= den; ++i){
                if(num%i==0 && den%i==0)
                    gcd = i;
    }
    return gcd;
}

struct fraction reduce_fraction (struct fraction f, int lcm){
    f.num /= lcm;
    f.den /= lcm;
    return f;
}

int main(){
    struct fraction a,b, result;
    a = scan(a);
    b = scan(b);
    result = compute (a,b);
    int lcm = gcd (result.num, result.den);
    result = reduce_fraction (result, lcm);
    printf("Reduced Fraction: %d / %d", result.num, result.den);
    return 0;
}
