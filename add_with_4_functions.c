//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
float scan(float x);
float add(float x, float y);
float display(float x, float y, float z);
int main(){
    float n1, n2, scanned1, scanned2, sum, displayed;
    scanned1 = scan(n1);
    scanned2 = scan(n2);
    printf("%.2f", scanned1);
    sum = add(scanned1, scanned2);
    displayed = display(scanned1, scanned2, sum);
    return 0;
}
float scan(float x){
    printf("Enter Number: ");
    scanf("%f", &x);
    return x;
}
float add(float x, float y){
    float addition = x + y;
    return addition;
}
float display(float x, float y, float z){
    printf("Sum of %.2f and %.2f is %.2f", x, y, z);
}