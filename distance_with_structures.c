//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include <math.h>
struct point{
    float x, y;
};
float compute(struct point p1, struct point p2){
    float distance;
    distance = sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) *(p1.y - p2.y));
    return distance;
}
int main(){
    struct point p1, p2;
    printf("Enter coordinates of 1st point (x,y):\n");
    scanf("%f", &p1.x);
    scanf("%f", &p1.y);
    printf("Enter coordinates of 2nd point (x,y):\n");
    scanf("%f", &p2.x);
    scanf("%f", &p2.y);
    printf("\nDistance between Points (%.2f, %.2f) and (%.2f, %.2f): %.2f\n", p1.x, p1.y, p2.x, p2.y, compute(p1, p2));
    return 0;
}