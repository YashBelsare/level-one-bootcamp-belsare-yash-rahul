//Write a program to add two user input numbers using one function.
#include <stdio.h>

int main()
{
    int n1, n2, result;
    
    printf("Enter Number: ");
    scanf("%d", &n1);
    printf("Enter Number: ");
    scanf("%d", &n2);
    
    result = n1 + n2;
    printf("Sum of %d and %d is %d", n1, n2, result);
    return 0;
}