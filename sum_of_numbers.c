//Write a program to find the sum of n different numbers using 4 functions
#include <stdio.h>
int scan(){
    int a;
    printf ("Enter number of elements: ");
    scanf ("%d", &a);
    return a;
}
int element(int n, int a[n]){
    printf("Enter the elements:\n\n");
    for (int i = 0; i < n; i++){
    	scanf("%d", &a[i]);
    }
}
int addition(int n, int a[n]){
    int sum = 0;
    for (int i = 0; i < n; i++){
        sum += a[i];
    }
    return sum;
}

int main(){
    int n, sum;
    n = scan();
    int a[n];
    element(n, a);
    sum = addition(n, a);
    printf("\nThe sum of %d numbers is: %d", n, sum);
    return 0;
}